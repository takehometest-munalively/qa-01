# QA - Test Player

# Introductions
Use this sample Flutter app: https://github.com/flutter/gallery
Write the end to end UI tests and expectations for a scenario where the user is adding some items to the shopping cart. An example flow would be:

## Requirements
● Opening the Shrine Gallery
● Adding the Walter henley (white) shirt to cart after going to the Clothing filter
● Adding the Shrug bag after using the Accessories filter
● Checking the total of the shopping cart
● Clearing the shopping cart

## Notes
Please note this is a mobile environment. You may choose the programming language of your choice. Also please have a README.md with instructions on how to run the tests and how you would setup the structure to fit automated tests in the SDLC knowing that we will be running ScrumBan. Submit your test to a source control of choice and give us the link or git bundle. A public repo is ok.
